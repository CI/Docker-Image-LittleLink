FROM ghcr.io/techno-tim/littlelink-server@sha256:42917c3ef616a679e637faf78b69231f815d57b6ab1fada06f2fb09f67c504e1

COPY OpenSans-VariableFont_wdth,wght.ttf /usr/src/app/build/public/css/fonts/opensans/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTS-muw.woff2
COPY OpenSans-VariableFont_wdth,wght.ttf /usr/src/app/build/public/css/fonts/opensans/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSCmu1aB.woff2
COPY OpenSans-VariableFont_wdth,wght.ttf /usr/src/app/build/public/css/fonts/opensans/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSGmu1aB.woff2